from setuptools import setup

setup(
    name='ed-talkflask',
    packages=['ed-talkflask'],
    include_package_data=True,
    install_requires=[
        'flask',
        'redis',
    ],
)