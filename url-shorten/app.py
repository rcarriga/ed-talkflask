from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect
import redis
import random

app = Flask(__name__)
database = redis.StrictRedis(host="localhost", charset="utf-8", port=6379, db=0, decode_responses=True)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/', methods=['POST'])
def generate_short_url():
    long_url = request.form['long_url']
    
    isAdded = False
    alphanumerics = 'abcdefghijklmnopqrstuvwxyz1234567890'
    key = ''.join(random.choice(alphanumerics) for i in range(11))    
    while not isAdded:
        key.join(random.choice(alphanumerics)) 
        if database.get(key) is None:
            isAdded = True
            database.set(key, long_url) 
    
    base_url = 'http://127.0.0.1:5000/'
    short_url = base_url + key

    return render_template('display.html', url=short_url)

@app.route('/<key>')
def go_to_long_url(key):
    long_url = database.get(key)
    if long_url is None:
        return render_template('error.html')
    else:
        return redirect(long_url) # bug right here.